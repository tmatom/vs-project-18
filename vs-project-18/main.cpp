//������
//���� ������ - ������� C++ ������ � Visual Studio(���������� ����������).
//
//1. ������� ����� Stack, ������� ����� ������������� ������� ������ ��������� ������ ����.������ ����� �� ������ ���� ���������(����������� ������������ ������).
//2. ����������� ��� ������ ���� ������ �� �����(int, float, double, string).������ ���� ��� ������� ������ pop(), ����� ������� �� ����� ������� �������, � push(), ����� �������� ����� �������.
//3. �������������� ���� ���������.
//4. ��� ������� ����� ��������� ����������� ������ ������� � ����������� ���� ��� ������ ��������(���������� ����� ������������ � ����������).
//
//������� �������� � ���� ������ �� ����������� �� gitlab ����� ����� ����� ��. (���������� �������� ������� ����� ������� visual studio)

#include <iostream>
#include <vector>
#include <string>

template<class T>
class Stack
{
public:
    void push(T);
    T pop();
    void show();
private:
    std::vector<T> v;
};

int main() {
    Stack<int> a;
    a.push(1);  a.push(2);  a.push(3);
    a.show();
    std::cout << "poped:" << a.pop() << std::endl;
    a.show();

    Stack<std::string> b;
    b.push("hello"); b.push("world");
    b.show();
    std::cout << "poped:" << b.pop() << std::endl;
    b.show();
    system("pause");
    return 0;
}

template<class T> void Stack<T>::push(T elem)
{
    v.push_back(elem);
}

template<class T> T Stack<T>::pop()
{
    T elem = v.back();
    v.pop_back();
    return elem;
}
template<class T> void Stack<T>::show()
{
    std::cout << "stack:";
    for (auto e : v) std::cout << e << " ";
    std::cout << std::endl;
}
//
//// ����� ������ � �������������� ������������� ����������� 
//// �� ������� ������ ��� �� ��������, ����� ��������� ������� � ��� �� ������
//
//// ���������� � ������������ �� ������ �������� ��� ������� ��� ��������� ������� �����.
//#include <type_traits>
//// C ++ �������� � ���� ��������� ��������� ���������, ������� ������������ ����������������, 
//// ������� �� �������� ����� � ���������� ��������� ����������� �������. 
//// ��� ���������� ����� ������� ��������� �� ��� ������: 
//// ���������� ��������� ������ � ���������� ������ ����������.
//#include <utility>
//
//#include <string>
//#include <vector>
//
//template<class TDes, class... TAcc>
//constexpr bool is_any_of = std::disjunction_v<std::is_same<TDes, TAcc>...>;
//
//template<typename T>
//class Stack
//{
//private:// ����� T ����� ��������� "����� ���� �� �����", ����� ���� ������� ������ � ����
//    std::vector <std::enable_if_t<is_any_of<T, int, float, double, std::string>, T>>  my_storage;
//
//public:
//    void push(T&& t)
//    {
//        my_storage.push_back(std::forward<T>(t));
//    }
//
//    void push(const T& t)
//    {
//        my_storage.push_back(std::forward<T>(t));
//    }
//
//    T pop()
//    {
//        T tmp = std::move(my_storage.back());
//        my_storage.pop_back();
//        return tmp;
//    }
//
//    unsigned size()
//    {
//        return my_storage.size();
//    }
//};